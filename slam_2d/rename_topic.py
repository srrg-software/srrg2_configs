from rosbag import Bag
with Bag('Y.bag', 'w') as Y:
    for topic, msg, t in Bag('new.bag'):
        Y.write('/scan' if topic == '/base_scan' else topic, msg, t)