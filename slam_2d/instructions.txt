1. Acquire dataset (rosbag record only odom, base_scan, tf)
2. In case you did not previously delete the others: 
- rosbag filter 2022-04-27-17-43-30.bag new.bag "topic in ['/odom', '/base_scan', '/tf']"
- rename base_scan (or change name of topic in configuration)
All in once: rosbag record -O 20220524_1530 /odom /tf /base_scan
3. ./slam_2d.srrg name.bag
