"SparseBlockLinearSolverCholeskyCSparse" { 
  "#id" : 1
 }

"MessageOdomSubsamplerSink" { 
  "#id" : 2, 

  // odometry topic to subsample
  "odom_topic" : "odom", 
  "push_sinks" : [ { 
  "#pointer" : 3
 } ], 

  // minimum rotation between odometries [rad]
  "rotation_min" : 0.100000001, 

  // name of the transform tree to subscribe to
  "tf_topic" : "tf", 

  // minimum translation between odometries [m]
  "translation_min" : 0.100000001
 }

"MultiLoopDetectorBruteForce2D" { 
  "#id" : 4, 
  "name" : "loop_detector", 

  // module used to figure out which local maps should be checked
  "local_map_selector" : { 
    "#pointer" : 5
   }, 

  // aligner used to register loop closures
  "relocalize_aligner" : { 
    "#pointer" : 6
   }, 

  // maximum chi per inlier for success [chi]
  "relocalize_max_chi_inliers" : 0.150000006, 

  // minimum number of inliers for success [int]
  "relocalize_min_inliers" : 500, 

  // minimum fraction of inliers over total correspondences [num_inliers/num_correspondences]
  "relocalize_min_inliers_ratio" : 0.699999988
 }

"AlignerSliceOdom2DPrior" { 
  "#id" : 7, 

  // name of the base frame in the tf tree
  "base_frame_id" : "", 
  "diagonal_info_matrix" : [ 100, 100, 100 ], 

  // name of the slice in the fixed scene
  "fixed_slice_name" : "odom", 

  // name of the sensor's frame in the tf tree
  "frame_id" : "", 

  // name of the slice in the moving scene
  "moving_slice_name" : "odom", 

  // robustifier used on this slice
  "robustifier" : { 
    "#pointer" : -1
   }
 }

"MultiTracker2D" { 
  "#id" : 8, 
  "name" : "tracker", 

  // computes relative transform between fixed and moving slices
  "aligner" : { 
    "#pointer" : 9
   }, 
  "push_sinks" : [  ], 
  "slice_processors" : [ { 
  "#pointer" : 10
 }, { 
  "#pointer" : 11
 } ], 

  // name of the transform tree to subscribe to
  "tf_topic" : "tf", 

  // name of the odom topic to propagate to the connected syncs
  "tracker_odom_topic" : "/tracker_odom"
 }

"SparseBlockLinearSolverCholmodFull" { 
  "#id" : 12
 }

"LocalMapSelectorBreadthFirst2D" { 
  "#id" : 5, 

  // min distance between local maps to start aggressive [int, distance on graph]
  "aggressive_relocalize_graph_distance" : 10, 

  // max distance to attempt global relocalize [int, distance on graph]
  "aggressive_relocalize_graph_max_range" : 20, 

  // factor to pimp the chi2 threshold depending on the lenght [float, magnitude]
  "aggressive_relocalize_range_increase_per_edge" : 0.0500000007, 

  // max distance in meters [float, magnitude]
  "max_local_map_distance" : 1, 

  // distance of candidate closures [int, magnitude]
  "relocalize_range_scale" : 2
 }

"CorrespondenceFinderProjective2f" { 
  "#id" : 13, 

  // min cosinus between normals
  "normal_cos" : 0.99000001, 

  // max distance between corresponding points
  "point_distance" : 0.5, 

  // projector to compute correspondences
  "projector" : { 
    "#pointer" : 14
   }
 }

"PointNormal2fProjectorPolar" { 
  "#id" : 15, 

  // end col angle    [rad]
  "angle_col_max" : 3.14159012, 

  // start col angle  [rad]
  "angle_col_min" : -3.14159012, 

  // end row angle    [rad]
  "angle_row_max" : 1.57079995, 

  // start row angle  [rad]
  "angle_row_min" : -1.57079995, 

  // maximum range [m]
  "range_max" : 5, 

  // minimum range [m]
  "range_min" : 0.300000012
 }

"PointNormal2fProjectorPolar" { 
  "#id" : 16, 

  // end col angle    [rad]
  "angle_col_max" : 3.14159012, 

  // start col angle  [rad]
  "angle_col_min" : -3.14159012, 

  // end row angle    [rad]
  "angle_row_max" : 1.57079995, 

  // start row angle  [rad]
  "angle_row_min" : -1.57079995, 

  // maximum range [m]
  "range_max" : 5, 

  // minimum range [m]
  "range_min" : 0.300000012
 }

"AlignerSliceProcessorLaser2D" { 
  "#id" : 17, 

  // name of the base frame in the tf tree
  "base_frame_id" : "", 

  // correspondence finder used in this cue
  "finder" : { 
    "#pointer" : 18
   }, 

  // name of the slice in the fixed scene
  "fixed_slice_name" : "points", 

  // name of the sensor's frame in the tf tree
  "frame_id" : "", 

  // minimum number of correspondences in this slice
  "min_num_correspondences" : 20, 

  // name of the slice in the moving scene
  "moving_slice_name" : "points", 

  // robustifier used on this slice
  "robustifier" : { 
    "#pointer" : 19
   }
 }

"SimpleTerminationCriteria" { 
  "#id" : 20, 

  // ratio of decay of chi2 between iteration
  "epsilon" : 0.00100000005
 }

"SparseBlockLinearSolverCholmodFull" { 
  "#id" : 21
 }

"CorrespondenceFinderNN2D" { 
  "#id" : 18, 

  // max distance for correspondences [meters]
  "max_distance_m" : 1, 

  // min cosinus between normals
  "normal_cos" : 0.800000012, 

  // resolution of the distance map [m/pixel]
  "resolution" : 0.0500000007
 }

"SimpleTerminationCriteria" { 
  "#id" : 22, 

  // ratio of decay of chi2 between iteration
  "epsilon" : 0.00999999978
 }

"SimpleTerminationCriteria" { 
  "#id" : 23, 

  // ratio of decay of chi2 between iteration
  "epsilon" : 0.00100000005
 }

"RobustifierCauchy" { 
  "#id" : 24, 

  // threshold of chi after which the kernel is active
  "chi_threshold" : 0.00999999978
 }

"RobustifierCauchy" { 
  "#id" : 19, 

  // threshold of chi after which the kernel is active
  "chi_threshold" : 0.0500000007
 }

"GridMapper2D" { 
  "#id" : 25, 
  "name" : "mapper", 

  // endpoint gain, the frequency is summed by this on endpoint
  "endpoint_gain" : 5, 

  // endpoint diameter, the endpoint is enlarged to this size [m]
  "endpoint_radius" : 0, 

  // angle for rendering the map [rad]
  "global_map_orientation" : 0, 

  // max diameter of a local map
  "local_map_size" : 10, 

  // set to true if the log is corrupted and marks an error with max_range
  "max_range_invalid" : 1, 
  "push_sinks" : [  ], 

  // resolution
  "resolution" : 0.0199999996, 

  // name of the transform tree to subscribe to
  "tf_topic" : "", 

  // time window to store the messages
  "time_horizon" : 1, 

  // max usable range of a scan
  "usable_range" : 5
 }

"MessageROSBagSource" { 
  "#id" : 26, 
  "name" : "source", 

  // file to read
  "filename" : "cappero_laser_odom_diag_2020-05-06-16-26-03.bag", 
  "topics" : [ "base_scan", "odom", "tf" ], 

  // verbose
  "verbose" : 0
 }

"IterationAlgorithmGN" { 
  "#id" : 27, 

  // damping factor, the higher the closer to gradient descend. Default:0
  "damping" : 0
 }

"Solver" { 
  "#id" : 28, 
  "name" : "solver", 
  "actions" : [  ], 

  // pointer to the optimization algorithm (GN/LM or others)
  "algorithm" : { 
    "#pointer" : 29
   }, 

  // pointer to linear solver used to compute Hx=b
  "linear_solver" : { 
    "#pointer" : 1
   }, 
  "max_iterations" : [ 20 ], 

  // Minimum mean square error variation to perform global optimization
  "mse_threshold" : 0.00100000005, 
  "robustifier_policies" : [  ], 

  // term criteria ptr, if 0 solver will do max iterations
  "termination_criteria" : { 
    "#pointer" : 22
   }, 

  // turn it off to make the world a better place
  "verbose" : 0
 }

"Solver" { 
  "#id" : 30, 
  "actions" : [  ], 

  // pointer to the optimization algorithm (GN/LM or others)
  "algorithm" : { 
    "#pointer" : 31
   }, 

  // pointer to linear solver used to compute Hx=b
  "linear_solver" : { 
    "#pointer" : 12
   }, 
  "max_iterations" : [ 10 ], 

  // Minimum mean square error variation to perform global optimization
  "mse_threshold" : -1, 
  "robustifier_policies" : [  ], 

  // term criteria ptr, if 0 solver will do max iterations
  "termination_criteria" : { 
    "#pointer" : 23
   }, 

  // turn it off to make the world a better place
  "verbose" : 0
 }

"RawDataPreprocessorOdom2D" { 
  "#id" : 32, 

  // topic of odom, to be blasted
  "topic" : "odom"
 }

"MultiAligner2D" { 
  "#id" : 9, 

  // toggles additional inlier only runs if sufficient inliers are available
  "enable_inlier_only_runs" : 0, 

  // toggles removal of correspondences which factors are not inliers in the last iteration
  "keep_only_inlier_correspondences" : 0, 

  // maximum number of iterations
  "max_iterations" : 10, 

  // minimum number ofinliers
  "min_num_inliers" : 10, 
  "slice_processors" : [ { 
  "#pointer" : 7
 }, { 
  "#pointer" : 33
 } ], 

  // this solver
  "solver" : { 
    "#pointer" : 34
   }, 

  // termination criteria, not set=max iterations
  "termination_criteria" : { 
    "#pointer" : -1
   }
 }

"LocalMapSplittingCriterionDistance2D" { 
  "#id" : 35, 
  "name" : "splitting_criterion", 

  // distance between the center of the local maps (in meters)
  "local_map_distance" : 1
 }

"Solver" { 
  "#id" : 34, 
  "actions" : [  ], 

  // pointer to the optimization algorithm (GN/LM or others)
  "algorithm" : { 
    "#pointer" : 27
   }, 

  // pointer to linear solver used to compute Hx=b
  "linear_solver" : { 
    "#pointer" : 21
   }, 
  "max_iterations" : [ 1 ], 

  // Minimum mean square error variation to perform global optimization
  "mse_threshold" : -1, 
  "robustifier_policies" : [  ], 

  // term criteria ptr, if 0 solver will do max iterations
  "termination_criteria" : { 
    "#pointer" : 20
   }, 

  // turn it off to make the world a better place
  "verbose" : 0
 }

"MultiGraphSLAM2D" { 
  "#id" : 36, 
  "name" : "slam", 

  // validator used to confirm loop closures
  "closure_validator" : { 
    "#pointer" : 37
   }, 

  // global solver for loop closures
  "global_solver" : { 
    "#pointer" : 28
   }, 

  // initialization algorithm
  "initializer" : { 
    "#pointer" : -1
   }, 

  // detector used to produce loop closures
  "loop_detector" : { 
    "#pointer" : 4
   }, 
  "push_sinks" : [ { 
  "#pointer" : 25
 }, { 
  "#pointer" : 38
 } ], 

  // relocalizer to avoid creation of new nodes
  "relocalizer" : { 
    "#pointer" : 39
   }, 

  // heuristics that determine when a new local map has to be generated
  "splitting_criterion" : { 
    "#pointer" : 35
   }, 

  // name of the transform tree to subscribe to
  "tf_topic" : "tf", 

  // incremental tracker
  "tracker" : { 
    "#pointer" : 8
   }
 }

"MapListener" { 
  "#id" : 38, 
  "name" : "listener", 
  "push_sinks" : [  ], 

  // name of the transform tree to subscribe to
  "tf_topic" : ""
 }

"NormalComputator1DSlidingWindowNormal" { 
  "#id" : 40, 

  // max curvature
  "max_curvature" : 0.200000003, 

  // min number of points to compute a normal
  "normal_min_points" : 5, 

  // max normal point distance
  "normal_point_distance" : 0.300000012
 }

"IterationAlgorithmGN" { 
  "#id" : 29, 

  // damping factor, the higher the closer to gradient descend. Default:0
  "damping" : 0
 }

"MultiAligner2D" { 
  "#id" : 6, 

  // toggles additional inlier only runs if sufficient inliers are available
  "enable_inlier_only_runs" : 1, 

  // toggles removal of correspondences which factors are not inliers in the last iteration
  "keep_only_inlier_correspondences" : 1, 

  // maximum number of iterations
  "max_iterations" : 30, 

  // minimum number ofinliers
  "min_num_inliers" : 10, 
  "slice_processors" : [ { 
  "#pointer" : 17
 } ], 

  // this solver
  "solver" : { 
    "#pointer" : 30
   }, 

  // termination criteria, not set=max iterations
  "termination_criteria" : { 
    "#pointer" : -1
   }
 }

"SceneClipperProjective2D" { 
  "#id" : 41, 

  // projector used to remap the points
  "projector" : { 
    "#pointer" : 15
   }, 

  // resolution used to decimate the points in the scan on a grid [meters]
  "voxelize_resolution" : 0.0500000007
 }

"PointNormal2fUnprojectorPolar" { 
  "#id" : 42, 

  // end angle    [rad]
  "angle_max" : 2.35618997, 

  // start angle  [rad]
  "angle_min" : -2.35618997, 

  // minimum number of points in ball when computing a valid normal
  "normal_min_points" : -10, 

  // range of points considered while computing normal
  "normal_point_distance" : 0.200000003, 

  // number of laser beams
  "num_ranges" : 721, 

  // max laser range [m]
  "range_max" : 5, 

  // min laser range [m]
  "range_min" : 0.0299999993
 }

"MessageROSSource" { 
  "#id" : 43, 
  "name" : "source_ros", 
  "topics" : [ "tf", "/scan", "odom" ], 

  // verbose
  "verbose" : 0
 }

"MessageSynchronizedSink" { 
  "#id" : 3, 

  // name of the frame in the message pack
  "output_frame_id" : "pack_frame_id", 

  // name of the topic in the message pack
  "output_topic" : "pack", 
  "push_sinks" : [ { 
  "#pointer" : 36
 } ], 

  // name of the transform tree to subscribe to
  "tf_topic" : "tf", 

  // interval for the messages to be sinchronized
  "time_interval" : 0.10000000000000001, 
  "topics" : [ "odom", "base_scan" ]
 }

"AlignerSliceProcessorLaser2D" { 
  "#id" : 33, 

  // name of the base frame in the tf tree
  "base_frame_id" : "", 

  // correspondence finder used in this cue
  "finder" : { 
    "#pointer" : 13
   }, 

  // name of the slice in the fixed scene
  "fixed_slice_name" : "points", 

  // name of the sensor's frame in the tf tree
  "frame_id" : "", 

  // minimum number of correspondences in this slice
  "min_num_correspondences" : 0, 

  // name of the slice in the moving scene
  "moving_slice_name" : "points", 

  // robustifier used on this slice
  "robustifier" : { 
    "#pointer" : 24
   }
 }

"IterationAlgorithmGN" { 
  "#id" : 31, 

  // damping factor, the higher the closer to gradient descend. Default:0
  "damping" : 0
 }

"PipelineRunner" { 
  "#id" : 44, 
  "name" : "runner", 
  "push_sinks" : [ { 
  "#pointer" : 45
 } ], 

  // the source of the pipeline
  "source" : { 
    "#pointer" : 26
   }, 

  // name of the transform tree to subscribe to
  "tf_topic" : ""
 }

"FactorGraphClosureValidator" { 
  "#id" : 37, 

  // ratio to accept a closure after min checks
  "inlier_accept_ratio" : 0.699999988, 

  // chi2 of an inlier
  "inlier_chi" : 0.0500000007, 

  // ratio to reject a closure after min checks
  "inlier_reject_ratio" : 0.300000012, 

  // minimum number a closure is checked
  "min_times_checked" : 5, 

  // region around the endpoint of a closure to compute partitions
  "partition_expansion_range" : 5
 }

"MultiRelocalizer2D" { 
  "#id" : 39, 

  // aligner unit used to determine the best nearby local map for relocalization
  "aligner" : { 
    "#pointer" : -1
   }, 

  // max translation to attempt a jump
  "max_translation" : 3, 

  // maximum chi per inlier for sucessful relocalization
  "relocalize_max_chi_inliers" : 0.150000006, 

  // minimum number of inliers for sucessful relocalization
  "relocalize_min_inliers" : 200, 

  // minimum fraction of inliers out of the total correspondences
  "relocalize_min_inliers_ratio" : 0.699999988
 }

"MessagePlatformListenerSink" { 
  "#id" : 45, 
  "name" : "pipeline", 
  "push_sinks" : [ { 
  "#pointer" : 46
 } ], 

  // name of the transform tree to subscribe to
  "tf_topic" : ""
 }

"TrackerSliceProcessorPriorOdom2D" { 
  "#id" : 11, 

  // measurement adaptor used in the slice
  "adaptor" : { 
    "#pointer" : 32
   }, 

  // name of the base frame in the tf tree
  "base_frame_id" : "", 

  // name of the sensor frame in the tf tree
  "frame_id" : "", 

  // name of the slice in the moving scene
  "measurement_slice_name" : "odom", 

  // name of the slice in the fixed scene
  "scene_slice_name" : "odom"
 }

"PointNormal2fProjectorPolar" { 
  "#id" : 14, 

  // end col angle    [rad]
  "angle_col_max" : 3.14159012, 

  // start col angle  [rad]
  "angle_col_min" : -3.14159012, 

  // end row angle    [rad]
  "angle_row_max" : 1.57079995, 

  // start row angle  [rad]
  "angle_row_min" : -1.57079995, 

  // maximum range [m]
  "range_max" : 20, 

  // minimum range [m]
  "range_min" : 0.300000012
 }

"RawDataPreprocessorProjective2D" { 
  "#id" : 47, 

  // normal computator object
  "normal_computator_sliding" : { 
    "#pointer" : 40
   }, 

  // range_max [meters]
  "range_max" : 10, 

  // range_min [meters]
  "range_min" : 0, 

  // topic of the scan
  "scan_topic" : "base_scan", 

  // un-projector used to compute the scan from the cloud
  "unprojector" : { 
    "#pointer" : 42
   }, 

  // unproject voxelization resolution
  "voxelize_resolution" : 0.0199999996
 }

"MessageSortedSink" { 
  "#id" : 46, 
  "name" : "sorter", 

  // messages older than this lag that will be blasted, no matta what
  "oblivion_interval" : 5, 
  "push_sinks" : [ { 
  "#pointer" : 2
 }, { 
  "#pointer" : 25
 } ], 

  // name of the transform tree to subscribe to
  "tf_topic" : "tf", 

  // lag time to sort messages
  "time_interval" : 1, 

  // if set prints crap
  "verbose" : 0
 }

"TrackerSliceProcessorLaser2D" { 
  "#id" : 10, 

  // measurement adaptor used in the slice
  "adaptor" : { 
    "#pointer" : 47
   }, 

  // name of the base frame in the tf tree
  "base_frame_id" : "", 

  // clipper used in the slice
  "clipper" : { 
    "#pointer" : 41
   }, 

  // merger used for aligment of local maps in the slice
  "closure_merger" : { 
    "#pointer" : -1
   }, 

  // name of the sensor frame in the tf tree
  "frame_id" : "", 

  // name of the slice in the moving scene
  "measurement_slice_name" : "points", 

  // merger used for aligment of a measurement to a local map in the slice
  "merger" : { 
    "#pointer" : 48
   }, 

  // name of the slice in the fixed scene
  "scene_slice_name" : "points"
 }

"MergerProjective2D" { 
  "#id" : 48, 

  // max distance for merging the points in the scene and the moving
  "merge_threshold" : 0.200000003, 

  // projector to compute correspondences
  "projector" : { 
    "#pointer" : 16
   }
 }

"PipelineRunner" { 
  "#id" : 49, 
  "name" : "runner_live", 
  "push_sinks" : [ { 
  "#pointer" : 45
 } ], 

  // the source of the pipeline
  "source" : { 
    "#pointer" : 43
   }, 

  // name of the transform tree to subscribe to
  "tf_topic" : ""
 }

