"SceneClipperProjective2D" { 
  "#id" : 1, 

  // projector used to remap the points
  "projector" : { 
    "#pointer" : 2
   }, 

  // resolution used to decimate the points in the scan on a grid [meters]
  "voxelize_resolution" : 0.100000001
 }

"CorrespondenceFinderProjective2f" { 
  "#id" : 3, 

  // min cosinus between normals
  "normal_cos" : 0.800000012, 

  // max distance between corresponding points
  "point_distance" : 0.5, 

  // projector to compute correspondences
  "projector" : { 
    "#pointer" : 4
   }
 }

"PointNormal2fProjectorPolar" { 
  "#id" : 5, 

  // end col angle    [rad]
  "angle_col_max" : 3.14159012, 

  // start col angle  [rad]
  "angle_col_min" : -3.14159012, 

  // end row angle    [rad]
  "angle_row_max" : 1.57079995, 

  // start row angle  [rad]
  "angle_row_min" : -1.57079995, 

  // maximum range [m]
  "range_max" : 20, 

  // minimum range [m]
  "range_min" : 0.300000012
 }

"SimpleTerminationCriteria" { 
  "#id" : 6, 

  // ratio of decay of chi2 between iteration
  "epsilon" : 0.00100000005
 }

"RobustifierCauchy" { 
  "#id" : 7, 

  // threshold of chi after which the kernel is active
  "chi_threshold" : 0.00999999978
 }

"SparseBlockLinearSolverCholmodFull" { 
  "#id" : 8
 }

"PointNormal2fProjectorPolar" { 
  "#id" : 2, 

  // end col angle    [rad]
  "angle_col_max" : 3.14159012, 

  // start col angle  [rad]
  "angle_col_min" : -3.14159012, 

  // end row angle    [rad]
  "angle_row_max" : 1.57079995, 

  // start row angle  [rad]
  "angle_row_min" : -1.57079995, 

  // maximum range [m]
  "range_max" : 20, 

  // minimum range [m]
  "range_min" : 0.300000012
 }

"PointNormal2fProjectorPolar" { 
  "#id" : 4, 

  // end col angle    [rad]
  "angle_col_max" : 3.14159012, 

  // start col angle  [rad]
  "angle_col_min" : -3.14159012, 

  // end row angle    [rad]
  "angle_row_max" : 1.57079995, 

  // start row angle  [rad]
  "angle_row_min" : -1.57079995, 

  // maximum range [m]
  "range_max" : 20, 

  // minimum range [m]
  "range_min" : 0.300000012
 }

"Solver" { 
  "#id" : 9, 
  "actions" : [  ], 

  // pointer to the optimization algorithm (GN/LM or others)
  "algorithm" : { 
    "#pointer" : 10
   }, 

  // pointer to linear solver used to compute Hx=b
  "linear_solver" : { 
    "#pointer" : 8
   }, 
  "max_iterations" : [ 1 ], 

  // Minimum mean square error variation to perform global optimization
  "mse_threshold" : -1, 
  "robustifier_policies" : [  ], 

  // term criteria ptr, if 0 solver will do max iterations
  "termination_criteria" : { 
    "#pointer" : 6
   }, 

  // turn it off to make the world a better place
  "verbose" : 0
 }

"NormalComputator1DSlidingWindowNormal" { 
  "#id" : 11, 

  // max curvature
  "max_curvature" : 0.200000003, 

  // min number of points to compute a normal
  "normal_min_points" : 5, 

  // max normal point distance
  "normal_point_distance" : 0.300000012
 }

"MultiAligner2D" { 
  "#id" : 12, 

  // toggles additional inlier only runs if sufficient inliers are available
  "enable_inlier_only_runs" : 1, 

  // toggles removal of correspondences which factors are not inliers in the last iteration
  "keep_only_inlier_correspondences" : 0, 

  // maximum number of iterations
  "max_iterations" : 10, 

  // minimum number ofinliers
  "min_num_inliers" : 10, 
  "slice_processors" : [ { 
  "#pointer" : 13
 } ], 

  // this solver
  "solver" : { 
    "#pointer" : 9
   }, 

  // termination criteria, not set=max iterations
  "termination_criteria" : { 
    "#pointer" : -1
   }
 }

"MergerProjective2D" { 
  "#id" : 14, 

  // max distance for merging the points in the scene and the moving
  "merge_threshold" : 0.200000003, 

  // projector to compute correspondences
  "projector" : { 
    "#pointer" : 5
   }
 }

"MessageFileSink" { 
  "#id" : 15, 
  "name" : "sink", 

  // file to write
  "filename" : "pozzo.json", 
  "push_sinks" : [  ], 

  // name of the transform tree to subscribe to
  "tf_topic" : "", 

  // verbose
  "verbose" : 0
 }

"PipelineRunner" { 
  "#id" : 16, 
  "name" : "runner", 
  "push_sinks" : [ { 
  "#pointer" : 17
 } ], 

  // the source of the pipeline
  "source" : { 
    "#pointer" : 18
   }, 

  // name of the transform tree to subscribe to
  "tf_topic" : ""
 }

"RawDataPreprocessorProjective2D" { 
  "#id" : 19, 

  // normal computator object
  "normal_computator_sliding" : { 
    "#pointer" : 11
   }, 

  // range_max [meters]
  "range_max" : 1000, 

  // range_min [meters]
  "range_min" : 0, 

  // topic of the scan
  "scan_topic" : "/base_scan", 

  // un-projector used to compute the scan from the cloud
  "unprojector" : { 
    "#pointer" : 20
   }, 

  // unproject voxelization resolution
  "voxelize_resolution" : 0.0199999996
 }

"PointNormal2fUnprojectorPolar" { 
  "#id" : 20, 

  // end angle    [rad]
  "angle_max" : 3.14159012, 

  // start angle  [rad]
  "angle_min" : -3.14159012, 

  // minimum number of points in ball when computing a valid normal
  "normal_min_points" : 5, 

  // range of points considered while computing normal
  "normal_point_distance" : 0.200000003, 

  // number of laser beams
  "num_ranges" : 721, 

  // max range allowed for unprojection [m]
  "range_max" : 20, 

  // min range allowed for unprojection [m]
  "range_min" : 0.300000012
 }

"IterationAlgorithmGN" { 
  "#id" : 10, 

  // damping factor, the higher the closer to gradient descend. Default:0
  "damping" : 0
 }

"TrackerSliceProcessorLaser2D" { 
  "#id" : 21, 

  // measurement adaptor used in the slice
  "adaptor" : { 
    "#pointer" : 19
   }, 

  // name of the base frame in the tf tree
  "base_frame_id" : "", 

  // clipper used in the slice
  "clipper" : { 
    "#pointer" : 1
   }, 

  // merger used for aligment of local maps in the slice
  "closure_merger" : { 
    "#pointer" : -1
   }, 

  // name of the sensor frame in the tf tree
  "frame_id" : "", 

  // name of the slice in the moving scene
  "measurement_slice_name" : "scan_points", 

  // merger used for aligment of a measurement to a local map in the slice
  "merger" : { 
    "#pointer" : 14
   }, 

  // name of the slice in the fixed scene
  "scene_slice_name" : "scene_points"
 }

"MultiTracker2D" { 
  "#id" : 17, 
  "name" : "tracker", 

  // computes relative transform between fixed and moving slices
  "aligner" : { 
    "#pointer" : 12
   }, 
  "push_sinks" : [ { 
  "#pointer" : 15
 } ], 
  "slice_processors" : [ { 
  "#pointer" : 21
 } ], 

  // name of the transform tree to subscribe to
  "tf_topic" : "", 

  // name of the odom topic to propagate to the connected syncs
  "tracker_odom_topic" : "/tracker_odom"
 }

"MessageROSBagSource" { 
  "#id" : 18, 
  "name" : "source", 

  // file to read
  "filename" : "put_here_the_name_od_a_bag_with_scan_and_joint_state", 
  "topics" : [ "/base_scan" ], 

  // verbose
  "verbose" : 0
 }

"AlignerSliceProcessorLaser2D" { 
  "#id" : 13, 

  // name of the base frame in the tf tree
  "base_frame_id" : "/base_link", 

  // correspondence finder used in this cue
  "finder" : { 
    "#pointer" : 3
   }, 

  // name of the slice in the fixed scene
  "fixed_slice_name" : "scan_points", 

  // name of the sensor's frame in the tf tree
  "frame_id" : "", 

  // minimum number of correspondences in this slice
  "min_num_correspondences" : 10, 

  // name of the slice in the moving scene
  "moving_slice_name" : "scene_points", 

  // robustifier used on this slice
  "robustifier" : { 
    "#pointer" : 7
   }
 }

