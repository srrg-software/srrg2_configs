TODO:
To run the solver incrementally
1. download the datasets
2. run
 ./solver_incremental.srrg <config_file> <graph_file>


There are two classes of configs
solver_incremental_*.config, that implements the basic incremental solver
solver_incremental_*_star config, that implements the new cool hierarchical solver
